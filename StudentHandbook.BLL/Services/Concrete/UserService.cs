﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using StudentHandbook.BLL.Services.Abstract;
using StudentHandbook.Domain.Model;
using StudentHandbook.Domain.Model.Abstract;

namespace StudentHandbook.BLL.Services.Concrete
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task CreateUser(User user, string password)
        {
            if(user == null)
                throw new ArgumentException();

            user.UserId = Guid.NewGuid();
            var salt = new byte[128 / 8];

            GenerateSalt(salt);

            user.PasswordHash = HashFrom(password, salt);
            user.PasswordHashSalt = salt;

            _unitOfWork.Users.CreateUser(user);

            return _unitOfWork.SaveChangesAsync();
        }

        private string HashFrom(string password, byte[] salt)
        {
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
        }

        private static void GenerateSalt(byte[] salt)
        {
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
        }

        public List<User> GetAll()
        {
            return _unitOfWork.Users.GetAll();
        }

        public Task<List<User>> GetAllAsync()
        {
            return _unitOfWork.Users.GetAllAsync();
        }

        public bool UserExists(string userName, string password)
        {
            if (userName == null || password == null)
                return false;

            var user = _unitOfWork.Users.TryGetBy(userName);

            if (user == null)
                return false;

            var generatedPasswordHash = HashFrom(password, user.PasswordHashSalt);
            var realPasswordHash = user.PasswordHash;

            return generatedPasswordHash == realPasswordHash;
        }

        public Task<User> TryGetByAsync(string userName)
        {
           return _unitOfWork.Users.TryGetByAsync(userName);
        }
    }
}
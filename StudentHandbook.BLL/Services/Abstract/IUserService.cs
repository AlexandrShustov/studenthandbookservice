﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudentHandbook.Domain.Model;

namespace StudentHandbook.BLL.Services.Abstract
{
    public interface IUserService
    {
        Task CreateUser(User user, string password);

        List<User> GetAll();
        Task<List<User>> GetAllAsync();
        bool UserExists(string userName, string password);
        Task<User> TryGetByAsync(string userName);
    }
}
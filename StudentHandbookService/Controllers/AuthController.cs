﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StudentHandbook.BLL.Services.Abstract;
using StudentHandbook.Domain.Model;
using StudentHandbook.DTO.Dto;
using StudentHandbookService.Auth;

namespace StudentHandbookService.Controllers
{
    [AllowAnonymous]
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private IUserService _userService;
        private IMapper _mapper;

        public AuthController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("login")]
        public async Task Login([FromBody] UserDto.LoginUserDto userDto)
        {
            if (userDto == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                return;
            }

            var userExists = _userService.UserExists(userDto.UserName, userDto.Password);
            
            if (userExists)
            {
                Response.ContentType = "application/json";

                var user = await _userService.TryGetByAsync(userDto.UserName);
                var token = GenerateToken(user);

                await Response.WriteAsync(JsonConvert.SerializeObject(token, new JsonSerializerSettings { Formatting = Formatting.Indented }));

                return;
            }

            Response.StatusCode = (int)HttpStatusCode.Unauthorized;
        }

        private string GenerateToken(User user)
        {
            var issuedAt = DateTime.UtcNow;
            var expires = DateTime.UtcNow.AddMinutes(AuthOptions.Lifetime);

            var tokenHandler = new JwtSecurityTokenHandler();

            var claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, user.Role.ToString())
            });

            const string sec = AuthOptions.Key;

            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.
                SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));

            var signingCredentials = new Microsoft.IdentityModel.Tokens.
                SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);

            var token =
                tokenHandler.CreateJwtSecurityToken(
                    issuer: AuthOptions.Issuer,
                    audience: AuthOptions.Audience,
                    subject: claimsIdentity,
                    notBefore: issuedAt,
                    expires: expires,
                    signingCredentials: signingCredentials);

            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }
}
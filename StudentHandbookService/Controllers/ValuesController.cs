﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StudentHandbook.BLL.Services.Abstract;

namespace StudentHandbookService.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private IUserService _userService;

        public ValuesController(IUserService userService)
        {
            _userService = userService;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //[HttpGet]
        //[Route("users")]
        //public async Task<string> GetAllUsers()
        //{
        //    var users =  await _userService.GetAllAsync();

        //    return JsonConvert.SerializeObject(users);
        //}

        [HttpGet]
        [Route("users")]
        public string GetAllUsers()
        {
            var users =  _userService.GetAll();
            return JsonConvert.SerializeObject(users);
            
        }
    }
}

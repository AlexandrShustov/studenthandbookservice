﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StudentHandbook.BLL.Services.Concrete;
using StudentHandbook.Domain.Model;
using StudentHandbook.DTO.Dto;

namespace StudentHandbookService.Controllers
{
    [Route("api/users")]
    public class UsersController : Controller
    {
        private readonly UserService _userService;
        private readonly IMapper _mapper;

        public UsersController(UserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        public async void Register([FromBody] UserDto.NewUserDto userDto)
        {
            if (ModelState.IsValid)
                await _userService.CreateUser(_mapper.Map<User>(userDto), userDto.Password);
        }

        [HttpGet]
        [Authorize(Roles = "Teacher")]
        public UserDto GetUserBy(string userName)
        {
            return _mapper.Map<UserDto>(_userService.TryGetByAsync(userName));
        }
    }
}
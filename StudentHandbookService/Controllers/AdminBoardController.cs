﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace StudentHandbookService.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/manage")]
    public class AdminBoardController : Controller
    {
        [HttpGet]
        [Route("main")]
        public IActionResult ControlPanel()
        {
            return View();
        }
    }
}
﻿using StudentHandbook.Domain.Model;
using StudentHandbook.DTO.Dto;

namespace StudentHandbookService.Infrasructure
{
    public class MapperProfile : AutoMapper.Profile
    {
        public MapperProfile()
        {
            CreateMap<UserDto.NewUserDto, User>();
            CreateMap<User, UserDto>();
        }
    }
}
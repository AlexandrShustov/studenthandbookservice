﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace StudentHandbookService.Auth
{
    public class AuthOptions
    {
        public const string Key = @"401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727" +
                           "429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
        
        public const string Issuer = "StudentHandbookService";
        public const string Audience = "client"; // потребитель токена
        public const int Lifetime = 60 * 24;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
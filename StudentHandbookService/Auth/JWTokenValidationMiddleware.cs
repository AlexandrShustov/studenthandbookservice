﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;

namespace StudentHandbookService.Auth
{
    // ReSharper disable once InconsistentNaming
    public class JWTokenValidationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IHttpContextAccessor _contextAccessor;

        public JWTokenValidationMiddleware(RequestDelegate next, IHttpContextAccessor contextAccessor)
        {
            _next = next;
            _contextAccessor = contextAccessor;
        }

        private static bool TryRetrieveToken(IHeaderDictionary headers, out string token)
        {
            token = null;
            if (!headers.TryGetValue("Authorization", out var authzHeaders) || authzHeaders.Count() > 1)
            {
                return false;
            }

            var bearerToken = authzHeaders.ElementAt(0);
            token = bearerToken.StartsWith("Bearer ") ? bearerToken.Substring(7) : bearerToken;
            return true;
        }

        public async Task Invoke(HttpContext context)
        {
            HttpStatusCode statusCode;

            if (!TryRetrieveToken(context.Request.Headers, out var token))
            {
                statusCode = HttpStatusCode.Unauthorized;
                //allow requests with no token - whether a action method needs an authentication can be set with the claimsauthorization attribute
                await _next.Invoke(context);
                return;
            }

            SecurityToken securityToken;
            try
            {
                const string sec = AuthOptions.Key;

                var now = DateTime.UtcNow;
                var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));

                var handler = new JwtSecurityTokenHandler();

                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    ValidAudience = AuthOptions.Audience,
                    ValidIssuer = AuthOptions.Issuer,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    LifetimeValidator = LifeTimeIsValid,
                    IssuerSigningKey = securityKey
                };

                Thread.CurrentPrincipal = handler.ValidateToken(token, validationParameters, out securityToken);
                _contextAccessor.HttpContext.User = handler.ValidateToken(token, validationParameters, out securityToken);
                //await context.Response.WriteAsync(token);
            }
            catch (SecurityTokenValidationException)
            {
                statusCode = HttpStatusCode.Unauthorized;
                await _next.Invoke(context);
            }
            catch (Exception)
            {
                statusCode = HttpStatusCode.InternalServerError;
                await _next.Invoke(context);
            }

            await _next.Invoke(context);
        }

        public bool LifeTimeIsValid(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            if (expires == null)
                return false;

            return DateTime.UtcNow < expires;
        }
    }

    // ReSharper disable once InconsistentNaming
    public static class JWTokenValidationMiddlewareExtension
    {
        public static IApplicationBuilder UseJwTokenValidation(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JWTokenValidationMiddleware>();
        }
    }
}
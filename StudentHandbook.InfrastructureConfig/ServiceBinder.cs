﻿using Microsoft.EntityFrameworkCore;
using Ninject;
using StudentHandbook.BLL.Services.Abstract;
using StudentHandbook.BLL.Services.Concrete;
using StudentHandbook.DAL;
using StudentHandbook.DAL.Repositories;
using StudentHandbook.Domain.Model.Abstract;

namespace StudentHandbook.InfrastructureConfig
{
    public class ServiceBinder
    {
        private readonly IKernelConfiguration _configuration;

        public ServiceBinder(IKernelConfiguration kernelConfiguration)
        {
            _configuration = kernelConfiguration;
        }

        public void Bind()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDatabaseContext>();
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=StudentHandbook;Trusted_Connection=True;");

            _configuration.Bind<IUnitOfWork>().To<UnitOfWork>().InSingletonScope()
                .WithConstructorArgument("contextOptions", optionsBuilder.Options);

            _configuration.Bind<IUserService>().To<UserService>();
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudentHandbook.DTO.Dto
{
    public class UserDto
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime DateOfBirth { get; set; }

        public class NewUserDto
        {
            [Required]
            public string UserName { get; set; }

            [Required]
            public string FirstName { get; set; }

            [Required]
            public string LastName { get; set; }

            [Required]
            public string Password { get; set; }

            [Required]
            [Compare("Password")]
            public string PasswordConfirmation { get; set; }

            [Required]
            [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
            public string Email { get; set; }

            [Required]
            [RegularExpression(@"^\+(?:[0-9] ?){6,14}[0-9]$")]
            public string Phone { get; set; }

            [Required]
            [Range(typeof(DateTime), "1/1/2000", "1/1/3000")]
            public DateTime DateOfBirth { get; set; }
        }

        public class LoginUserDto
        {
            [Required]
            public string UserName { get; set; }

            [Required]
            public string Password { get; set; }
        }
    }
}
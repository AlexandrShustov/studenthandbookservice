﻿namespace StudentHandbook.Domain.Model
{
    public enum UserRole
    {
        Admin,

        Teacher,
        Student
    }
}
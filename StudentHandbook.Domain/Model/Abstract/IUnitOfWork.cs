﻿using System.Threading;
using System.Threading.Tasks;

namespace StudentHandbook.Domain.Model.Abstract
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
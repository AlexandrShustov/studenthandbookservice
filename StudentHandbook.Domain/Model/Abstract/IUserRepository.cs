﻿using System;
using System.Threading.Tasks;

namespace StudentHandbook.Domain.Model.Abstract
{
    public interface IUserRepository : IRepository<User>
    {
        void CreateUser(User user);
        User TryGetBy(string userName);
        Task<User> TryGetByAsync(string userName);
        User TryGetBy(Guid userId);
        Task<User> TryGetByAsync(Guid userId);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentHandbook.Domain.Model.Abstract
{
    public interface IRepository<TEntity> where TEntity: class
    {
        List<TEntity> GetAll();
        Task<List<TEntity>> GetAllAsync();
    }
}
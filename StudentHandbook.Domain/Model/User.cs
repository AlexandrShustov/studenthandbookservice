﻿using System;
using System.Collections;

namespace StudentHandbook.Domain.Model
{
    public class User
    {
        public Guid UserId { get; set; }
        public UserRole Role { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public byte[] PasswordHashSalt { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
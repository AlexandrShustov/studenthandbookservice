﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StudentHandbook.Domain.Model;
using StudentHandbook.Domain.Model.Abstract;

namespace StudentHandbook.DAL.Repositories
{
    public class UsersRepository : IUserRepository
    {
        private readonly ApplicationDatabaseContext _context;

        public UsersRepository(ApplicationDatabaseContext context)
        {
            _context = context;
        }

        public List<User> GetAll()
        {
            return _context.Users.ToList();
        }

        public Task<List<User>> GetAllAsync()
        {
            return _context.Users.ToListAsync();
        }

        public void CreateUser(User user)
        {
            _context.Users.Add(user);
        }

        public User TryGetBy(string userName)
        {
            return _context.Users.FirstOrDefault(user => user.UserName == userName);
        }

        public Task<User> TryGetByAsync(string userName)
        {
            return _context.Users.FirstOrDefaultAsync(user => user.UserName == userName);
        }

        public User TryGetBy(Guid userId)
        {
            return _context.Users.FirstOrDefault(user => user.UserId == userId);
        }

        public Task<User> TryGetByAsync(Guid userId)
        {
            return _context.Users.FirstOrDefaultAsync(user => user.UserId == userId);
        }
    }
}
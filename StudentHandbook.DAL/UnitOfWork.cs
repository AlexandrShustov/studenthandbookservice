﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StudentHandbook.DAL.Repositories;
using StudentHandbook.Domain.Model.Abstract;

namespace StudentHandbook.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDatabaseContext _context;

        private IUserRepository _userRepository;
        public IUserRepository Users => _userRepository ?? (_userRepository = new UsersRepository(_context));

        public UnitOfWork(DbContextOptions contextOptions)
        {
            _context = new ApplicationDatabaseContext(contextOptions);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using StudentHandbook.Domain.Model;

namespace StudentHandbook.DAL
{
    public class ApplicationDatabaseContext : DbContext
    {
        internal DbSet<User> Users { get; set; }

        public ApplicationDatabaseContext(DbContextOptions contextOptions)
        :base(contextOptions)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();

            CreateTestUsers();
        }

        private void CreateTestUsers()
        {
            var newUser = new User
            {
                UserId = Guid.NewGuid(),
                DateOfBirth = DateTime.Now,
                Email = "test@test.com",
                FirstName = "John",
                LastName = "Doe",
                Phone = "+380957500085",
                UserName = "username",
                Role = UserRole.Teacher
            };

            Users.Add(newUser);
            SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
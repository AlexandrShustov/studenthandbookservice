﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentHandbook.Domain.Model;

namespace StudentHandbook.DAL
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(user => user.UserId);

            builder.Property(user => user.UserId)
                .HasColumnName("UserId")
                .HasColumnType("uniqueidentifier")
                .IsRequired();

            builder.Property(user => user.UserName)
                .HasColumnName("UserName")
                .IsRequired();

            builder.Property(user => user.Role)
                .HasColumnName("UserRole")
                .IsRequired();
            
            builder.Property(user => user.PasswordHash)
                .HasColumnName("PasswordHash")
                .IsRequired();

            builder.Property(user => user.PasswordHashSalt)
                .HasColumnName("PasswordHashSalt")
                .IsRequired();

            builder.Property(user => user.DateOfBirth)
                .HasColumnName("Birthday")
                .HasColumnType("date")
                .IsRequired();

            builder.Property(user => user.Email)
                .HasColumnName("Email")
                .IsRequired();

            builder.Property(user => user.FirstName)
                .HasColumnName("FirstName")
                .IsRequired();

            builder.Property(user => user.LastName)
                .HasColumnName("LastName")
                .IsRequired();

            builder.Property(user => user.Phone)
                .HasColumnName("Phone")
                .IsRequired();
        }
    }
}